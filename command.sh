#!/bin/bash

export VAPORMAP_BACKEND

export VAPORMAP_BACKEND_PORT

envsubst '${VAPORMAP_BACKEND},${VAPORMAP_BACKEND_PORT}' < config.json.template > config.json

exec "$@"







